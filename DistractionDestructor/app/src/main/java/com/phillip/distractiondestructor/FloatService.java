package com.phillip.distractiondestructor;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;


public class FloatService extends Service {


    private WindowManager mWindowManager;
    private View mOverlayView;
    CustomFAB customFAB;
//    boolean activity_background;
    final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Starting a new session", Toast.LENGTH_SHORT).show();
//        if (intent != null) {
//            activity_background = intent.getBooleanExtra("activity_background", false);
//        }

        if (mOverlayView == null) {

            mOverlayView = LayoutInflater.from(this).inflate(R.layout.overlay_layout, null);

            //Specify the view position
            params.gravity = Gravity.TOP | Gravity.LEFT;        //Initially view will be added to top-left corner
            params.x = 0;
            params.y = 100;


            mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            mWindowManager.addView(mOverlayView, params);

            customFAB = mOverlayView.findViewById(R.id.fab);

            final CoordinatorLayout layout = mOverlayView.findViewById(R.id.layout);
            ViewTreeObserver vto = layout.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });

            setFabListeners();
        }


        return super.onStartCommand(intent, flags, startId);
    }


    private void setFabListeners() {
        customFAB.setOnTouchListener(new View.OnTouchListener() {
            private final static float CLICK_DRAG_TOLERANCE = 10;
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = motionEvent.getRawX();
                        initialTouchY = motionEvent.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
//                            if (activity_background) {    //is this ever necessary?
                        float upDX = motionEvent.getRawX() - initialTouchX;
                        float upDY = motionEvent.getRawY() - initialTouchY;

                        if (Math.abs(upDX) < CLICK_DRAG_TOLERANCE && Math.abs(upDY) < CLICK_DRAG_TOLERANCE) { // A click
                            return customFAB.performClick();
                        } else { // A drag
                            mWindowManager.updateViewLayout(mOverlayView, params);
                            return true; // Consumed
                        }
//                            }
                    case MotionEvent.ACTION_MOVE:
                        int xDiff = Math.round(motionEvent.getRawX() - initialTouchX);
                        int yDiff = Math.round(motionEvent.getRawY() - initialTouchY);

                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + xDiff;
                        params.y = initialY + yDiff;

                        //Update the layout with new X & Y coordinates
                        mWindowManager.updateViewLayout(mOverlayView, params);
                        return true;
                }
                return false;
            }
        });

        customFAB.setOnClickListener(v -> {
            Toast.makeText(this, "Clicked FAB", Toast.LENGTH_SHORT).show();
        });

    }

    @Override
    public void onCreate() {
        super.onCreate();

        setTheme(R.style.Theme_MaterialComponents);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOverlayView != null)
            mWindowManager.removeView(mOverlayView);
    }
}

