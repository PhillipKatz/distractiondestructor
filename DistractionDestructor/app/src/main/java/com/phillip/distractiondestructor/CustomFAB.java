package com.phillip.distractiondestructor;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;


public class CustomFAB extends ExtendedFloatingActionButton {
    // fixes accidental drags meant to be clicks

    public CustomFAB(Context context) { super(context); }

    public CustomFAB(Context context, AttributeSet attrs) { super(context, attrs); }

    public CustomFAB(Context context, AttributeSet attrs, int defStyleAttr) { super(context, attrs, defStyleAttr); }

}
